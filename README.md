# tw-diagram #

A simple plugin for tiddlywiki to create inline diagrams inspired by tikz/pgf in LaTeX.

## Why tw-diagram ##

I consider using the [Tamasha](https://kookma.github.io/TW-Tamasha/) plugin for presentations.
Since I frequently use simple diagrams I was looking for a simple way to draw them inline.

This plugin provides the basic notion of inlne diagrams

## How tw-diagram ##

Here you find here the source files:

* plugins/* contains the plugin file that usually reside in TW5/plugins/hydiga/diagram
* tiddlers/* the tiddlers used in the demonstration tiddlywiki

## Demonstration ##

A static demonstration is avaiable at: [bitbucket.io](https://hwvandijk.bitbucket.io/tw-diagram/)

## Installing the plugin ##

You can drag and drop the plugin from [dataframe tiddlywiki](https://hwvandijk.bitbucket.io/tw-diagram/).

* Open the configuration,
* select plugins, and
* drag and drop diagram plugin.

